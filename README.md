#Creditas

Desafio para QA - Test Engineer
===================

Desafio para especificar/descrever uma funcionalidade e avaliar o conhecimento em automação de testes com Ruby e Cucumber.


**Sistema Operacional:** 

 - Windows 10

**Programas utilizados:**

 - Visual Studio Code - versão: 1.25.1
  
 - Cmder

----------


1. Especificação 
-------------

``` 
#language: pt

@login_creditas
Funcionalidade: Efetuar login no site da créditas

Contexto: acessar o site
Dado que usuário possa afetuar o acesso a tela de login

Esquema do Cenário: Efetuar login no site com usuários válidos
Quando inserir um <Email> ou <CPF> válido
E inserir uma <Senha> válida
Então o sistema deve permitir o login do usuário no site

Exemplos:
| Email                 |CPF          | Senha   |
| "junior@creditas.com" |"12312312312"| "12345" |
| "aline@creditas.com"  |"34334334343"| "54321" |
| "jilvan@creditas.com" |"76756776756"| "45678" |
```

2 - Automação de testes funcionais
-------------
2.1 Adicionar e remover o checkbox

```
#language: pt

@checkbox
Funcionalidade: adicionar e remover o checkbox

Cenário: remover checkbox
Quando eu clico no botão
Então verifico se o checkbox foi removido.

Cenário: adicionar checkbox
Quando clico no botão
Então verifico se o checkbox foi adicioando.
```

2.2 Nova aba
```
#language: pt

@open_window
Funcionalidade: Abrir nova Janela

Cenário: Abrindo nova janela
Quando eu clico no link
Então verifico que abriu uma nova
```
